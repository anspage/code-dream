defmodule CodeDream.Repo do
  use Ecto.Repo,
    otp_app: :code_dream,
    adapter: Ecto.Adapters.Postgres
end
