defmodule CodeDream.Application do
  use Application

  def start(_type, _args) do
    children = [
      CodeDream.Repo,
      CodeDreamWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: CodeDream.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    CodeDreamWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
