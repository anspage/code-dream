defmodule CodeDreamWeb.Resolvers.User do
  alias CodeDream.{Repo, User, Session}

  def all(_args, %{context: %{current_user: %{id: id}}}) do
    {:ok, Repo.all(User)}
  end

  def all(_args, _info) do
    {:error, "Not Authorized"}
  end

  def find(%{id: id}, %{context: %{current_user: %{id: uid}}}) do
    case Repo.get(User, id) do
      nil  -> {:error, "User id #{id} not found"}
      user -> {:ok, user}
    end
  end

  def find(_args, _info) do
    {:error, "Not Authorized"}
  end

  def update(%{id: id, user: user_params}, %{context: %{current_user: %{id: id}}}) do
    User
    |> Repo.get!(id)
    |> User.changeset(user_params)
    |> Repo.update
  end

  def update(_args, _info) do
    {:error, "Not Authorized"}
  end

  def login(params, _info) do
    with {:ok, user} <- Session.authenticate(params),
         {:ok, jwt, _ } <- Guardian.encode_and_sign(user, :access) do
      {:ok, %{token: jwt}}
    end
  end
end
