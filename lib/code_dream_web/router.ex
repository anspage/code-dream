defmodule CodeDreamWeb.Router do
  use CodeDreamWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CodeDreamWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  pipeline :graphql do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
    plug CodeDreamWeb.Context
  end

  scope "/api" do
    pipe_through :graphql

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: CodeDreamWeb.Schema

    forward "/", Absinthe.Plug,
      schema: CodeDreamWeb.Schema
  end
end
