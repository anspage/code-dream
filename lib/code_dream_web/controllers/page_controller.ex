defmodule CodeDreamWeb.PageController do
  use CodeDreamWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
