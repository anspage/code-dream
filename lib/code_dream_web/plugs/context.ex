defmodule CodeDreamWeb.Context do
  @behaviour Plug

  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    case Guardian.Plug.current_resource(conn) do
      user -> put_private(conn, :absinthe, %{context: %{current_user: user}})
      nil  -> conn
    end
  end
end
