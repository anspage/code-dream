defmodule CodeDreamWeb.Schema.User do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: CodeDream.Repo
  import CodeDreamWeb.Schema.Error, only: [handle_errors: 1]

  alias CodeDreamWeb.Resolvers

  @doc "User objects"

  object :user do
    field :id, :id
    field :first_name, :string
    field :last_name, :string
    field :email, :string
  end

  object :session do
    field :token, :string
  end

  input_object :update_user_params do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :password, :string
    field :password_confirmation, :string
  end

  @doc "User queries"

  object :user_queries do

    @desc "Get all users"
    field :users, list_of(:user) do
      resolve &Resolvers.User.all/2
    end

    @desc "Get user"
    field :user, type: :user do
      arg :id, non_null(:id)
      resolve &Resolvers.User.find/2
    end
  end

  @doc "User mutations"

  object :user_mutations do

    @desc "Update user"
    field :update_user, type: :user do
      arg :id, non_null(:integer)
      arg :user, :update_user_params

      resolve handle_errors(&Resolvers.User.update/2)
    end

    @desc "User login"
    field :login, type: :session do
      arg :email, non_null(:string)
      arg :password, non_null(:string)

      resolve handle_errors(&Resolvers.User.login/2)
    end
  end
end
