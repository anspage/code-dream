// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from '../css/app.css'

import 'phoenix_html'
import Vue from 'vue'

// Custom components
import Root from '../components/Root.vue'

new Vue({
  el: '#app',
  render: h => h(Root)
})
