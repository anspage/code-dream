# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :code_dream,
  ecto_repos: [CodeDream.Repo]

# Configures the endpoint
config :code_dream, CodeDreamWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FmosMVp4evExicZ0+8Gu3sKUoTOCYEAbB2mf4plaYhov4QstBwL2ZFQINER6M2Xy",
  render_errors: [view: CodeDreamWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CodeDream.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures Postgres
config :code_dream, Repo,
  url: System.get_env("DATABASE_URL")

# Configures Guardian
config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "CodeDream",
  ttl: { 30, :days },
  verify_issuer: true,
  secret_key: "5wzKjAcLrp8KWv9DGaaBykNyZHJyko3g24sfPoKdEn32/DvMNmLeW953",
  serializer: CodeDreamWeb.Serializers.Guardian

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
